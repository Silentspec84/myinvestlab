#!/bin/bash

chmod 600 /etc/cron.d/cron
chmod 600 /root/.ssh/id_rsa

service cron start
sleep 3
echo "Init cron task"
crontab /etc/cron.d/cron

set -e

worker() {
  echo "Starting worker"
  cp /keys /root/.ssh/authorized_keys
  chown root. /root/.ssh/authorized_keys
  exec /usr/sbin/sshd -D
}

case "$1" in
  worker)
    shift
    worker
    ;;
  *)
    exec "$@"
    ;;
esac