<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name'=>'myinvestLab.ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@logs' => '@app/logs',
        '@uploads' => '@app/resources/uploads',
        '@web' => '@app/web',
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => 'xVmLSTJIXk52nKA20Sh5LX5GDHly37j-',
            'enableCsrfValidation' => true,
            'enableCookieValidation' => true,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [
                '/' => 'site/index',
                '<controller:[\w\-]+>/<id:\d+[/]*>' => '<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+[/]*>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+[/]*>' => '<controller>/<action>',
                '<action:(recover|recovery|register-success|confirm-employee|confirm-client|confirm-email)[/]*>' => 'site/<action>',
//                '<controller:[-\w]+>/<action:[-\w]+>/<page:\d+>' => '<controller>/<action>',
//                '<controller:[-\w]+>/<action:[-\w]+>/<id:\d+>/<page:\d+>' => '<controller>/<action>',
//                '<controller:[-\w]+>/<action:[-\w]+>' => '<controller>/<action>',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'trade-api',
                    'extraPatterns' => [
                        'POST auth' => 'auth',
                        'POST first-connect' => 'first-connect',
                        'POST refresh-account' => 'refresh-account',
                        'POST history' => 'history',
                        'POST opened' => 'opened',
                    ],
                ],
            ],
//            [
//                'pattern' => 'sitemap',
//                'route' => 'site/sitemap',
//                'suffix' => '.xml'
//            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'viewPath' => '@app/mail',
            'htmlLayout' => 'layouts/html',
            'useFileTransport' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['profxteam777@gmail.com' => 'InvestLab.ru'],
            ],
            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.gmail.com',
//                'username' => 'b2b.anyport@gmail.com',
//                'password' => 'AnyportB2B',
//                'port' => '587',
//                'encryption' => 'tls',
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'profxteam777@gmail.com',
                'password' => '09111984-=QWEpoi',
                'port' => '465',
                'encryption' => 'ssl',
                'streamOptions' => [
                    'ssl' => [
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ]
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget', // будем писать в файл
                    'exportInterval' => 1,  // по умолчанию 1000
                    'levels' => ['info'], // будем писать, когда вызван метод ifno
                    'categories' => ['application'], // будем писать для категории application (передается в метод вторым параметром, 'application' - значение по умолчанию)
                    'logFile' => '@app/logs/success.log', // будем писать в этот файл
                    'logVars' => [], // запишем переменные, перечисленные в этом массиве (например, можно положить сюда $_POST)
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'exportInterval' => 1,  // по умолчанию 1000
                    'levels' => ['error', 'warning'],
                    'categories' => ['application'],
                    'logFile' => '@app/logs/error.log',
                    'logVars' => [],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
