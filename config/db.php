<?php

$db = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=mysql;dbname=myinvestlab',
    'username' => 'root',
    'password' => '123123',
    'charset' => 'utf8',
];

if (file_exists(__DIR__ . '/db-main.php')) {
    $db = yii\helpers\ArrayHelper::merge(
        $db,
        require(__DIR__ . '/db-main.php')
    );
}

return $db;
