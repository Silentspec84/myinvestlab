<?php

namespace app\commands;

use app\classes\helpers\InstrumentParserHelper;
use app\models\Exchange;
use app\models\Logs;
use app\models\PairChain;
use Yii;
use yii\console\Controller;
use yii\helpers\Json;

class DaemonChainsController extends Controller
{

    /** @var Logs $logger  */
    private $logger = null;

    /** @var Exchange $exchange  */
    private $exchange = null;

    public function beforeAction($action)
    {
        $exchange_id = Yii::$app->request->params[1];
        $this->exchange = Exchange::findOne($exchange_id);
        $this->logger = Logs::getLogger($this->exchange->id, Logs::CALC_CHAINS, true);
        return parent::beforeAction($action);
    }
    /**
     * Просчитывает все возможные цепочки сделок для внутрибиржевого арбитража
     * @param $id - id биржи
     * @return string
     */
    public function actionRun($id) {

        $this->logger->saveMessage('Удаляем все существующие цепочки');
        if (PairChain::deleteAll(['exchange_id' => $this->exchange->id]) < 0) {
            $this->logger->saveMessage('Не удалось удалить существующие цепочки');
            return;
        }

        $this->logger->saveMessage('Создаем класс парсера цепочек');
        $parser = new InstrumentParserHelper($this->exchange, $this->logger);

        $this->logger->saveMessage('Рассчитаем тройки');
        $chains['tripples'] = $parser->parseTripples();
        foreach ($chains['tripples'] as $chain) {
            $result[] = [
                $this->exchange->id, PairChain::TYPE_TRIPPLES,
                $chain['pattern'], Json::encode($chain['base']),
                Json::encode($chain['dir']), $chain['first_currency'],
                PairChain::STATUS_ON, time(), time()
            ];
        }
        if (!PairChain::saveChains($result)) {
            $this->logger->saveMessage('Не удалось сохранить цепочки');
            return;
        }
        $this->logger->saveMessage('Троек ' . count($chains['tripples']));

        $this->logger->saveMessage('Рассчитаем четверки');
        $chains['quadripples'] = $parser->parseQuadripples();
        $result = [];
        foreach ($chains['quadripples'] as $chain) {
            $result[] = [
                $this->exchange->id, PairChain::TYPE_QUADRIPPLES,
                $chain['pattern'], Json::encode($chain['base']),
                Json::encode($chain['dir']), $chain['first_currency'],
                PairChain::STATUS_ON, time(), time()
            ];
        }
        $this->logger->saveMessage('Четверок ' . count($chains['quadripples']));
        $count_result = (int)round(count($result) / 1000, 0);

        for ($count = 0; $count <= $count_result; $count++) {
            $result_to_save = array_slice($result, $count * 1000, 1000);
            if (!PairChain::saveChains($result_to_save)) {
                $this->logger->saveMessage('Не удалось сохранить цепочки');
                return;
            }
        }

        $this->logger->saveMessage('Цепочки успешно сохранены');
        return;
    }
}