<?php

namespace app\commands;

use app\classes\connectors\ApiConnectorInterface;
use app\classes\strategies\StrategyInterface;
use app\models\Daemon;
use app\models\Exchange;
use app\models\Logs;
use Yii;
use yii\console\Controller;

class DaemonTradeController extends Controller
{
    /** @var Daemon $daemon  */
    private $daemon = null;

    private $daemon_id = null;

    /** @var Logs $logger  */
    private $logger = null;

    /** @var Exchange $exchange  */
    private $exchange = null;

    /** @var ApiConnectorInterface $api  */
    private $api = null;

    /** @var StrategyInterface $strategy  */
    private $strategy = null;

    public function beforeAction($action)
    {
        $daemon_id = Yii::$app->request->params[1];
        $this->daemon = Daemon::findOne($daemon_id);
        $this->daemon_id = $this->daemon->id;
        $this->exchange = Exchange::findOne($this->daemon->exchange_id);
        $this->logger = Logs::getLogger($this->exchange->id, 'Daemon ' . $this->daemon->id . ' (' . Daemon::$daemon_types[$this->daemon->job_type] . ')', true);
        $this->api = new $this->exchange->class($this->logger, $this->exchange, true);
        $this->strategy = new $this->daemon->strategy($this->exchange, $this->api, $this->daemon->job_type, $this->logger);
        if (!$this->strategy->ready) {
            $this->logger->saveMessage('Не удалось создать стратегию!');
            return false;
        }
        return parent::beforeAction($action);
    }

    public function actionRun($id)
    {
        $count = 1;
        while(true) {
            // Очищаем сообщения логгера для нового круга, обновляем параметры демона
            $this->logger->clearMessage();
            $this->daemon = Daemon::findOne($this->daemon_id);
            if (!$this->daemon->enabled) {
                $this->logger->saveMessage('Получили команду на стоп, выходим');
                break;
            }
            $this->logger->saveMessage('Запускаем стратегию. Запуск №' . $count);

            $this->strategy->getPrices();
            $this->strategy->calculate();
            $this->logger->saveMessage('Круг ' . $count . ' успешно завершен, сохраним данные');

            $this->daemon->date_last_connected = time();
            if (!$this->daemon->save()) {
                $this->logger->saveMessage('Ошибка сохранения данных демона, выходим');
                break;
            }
            sleep($this->daemon->timer);
            $count++;
        }
    }

}