<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/semantic.min.css',
    ];
    public $js = [
        'https://code.jquery.com/jquery-3.4.1.min.js',
        'js/semantic.min.js',
        'js/plugins/axios.js',
        'https://unpkg.com/vue@2.6.11/dist/vue.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
