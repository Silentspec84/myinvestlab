<?php

namespace app\components;

use app\classes\helpers\ConfigHelper;
use app\models\Company;
use Yii;
use yii\base\Widget;

/**
 * Виджет отвечает за вывод кастомного хедера
 * Class Header
 * @package app\components
 */
class Header extends Widget
{
    private $user;

    public function init()
    {
        parent::init();

        if (isset(Yii::$app)) {
            $this->user = Yii::$app->getUser()->identity;
        }
    }

    public function run()
    {
        return $this->render('header', ['user' => $this->user]);
    }

}