<?php
?>
<!-- Following Menu -->
<div class="ui large top fixed hidden menu">
    <div class="ui container">
        <div class="ui simple dropdown item">
            Организации <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item">Эмитенты</a>
                <a class="item">Биржи</a>
                <a class="item">Брокеры</a>
                <a class="item">Регуляторы</a>
            </div>
        </div>
        <div class="ui simple dropdown item">
            Инструменты <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item">Акции</a>
                <a class="item">Облигации</a>
                <a class="item">Взаимные фонды</a>
                <a class="item">ETF</a>
                <a class="item">Товары</a>
                <a class="item">Индексы</a>
                <a class="item">Валюты</a>
                <a class="item">Криптовалюты</a>
                <a class="item">Сертификаты</a>
            </div>
        </div>
        <div class="ui simple dropdown item">
            Портфель <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item">Мои портфели</a>
                <a class="item">Анализ портфеля</a>
                <a class="item">Оптимизация портфеля</a>
                <a class="item">Поиск корреляций</a>
                <a class="item">Маркет тайминг</a>
                <a class="item">Анализ риска</a>
            </div>
        </div>
        <div class="ui simple dropdown item">
            Анализ рынка <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item">Лидеры роста и падения</a>
                <a class="item">Сделки инсайдеров</a>
                <a class="item">Инвестиционные идеи</a>
                <a class="item">Анализ акций</a>
                <a class="item">Анализ облигаций</a>
                <a class="item">Анализ дивидендов</a>
                <a class="item">Анализ отраслей</a>
            </div>
        </div>
        <div class="ui simple dropdown item">
            Калькуляторы <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item">Калькулятор инфляции</a>
                <a class="item">Калькулятор пенсии</a>
            </div>
        </div>
        <div class="ui simple dropdown item">
            Прочее <i class="dropdown icon"></i>
            <div class="menu">
                <a class="item">Корпоративные новости</a>
                <a class="item">Собрания акционеров</a>
                <a class="item">Календарь дивидендов</a>
            </div>
        </div>
        <a class="active item" href="https://myinvestlab.ru/" target="_blank">Блог</a>
        <a class="active item" href="https://myinvestlab.ru/forum/" target="_blank">Форум</a>
<!--        <div class="right menu">-->
<!--            <div class="item">-->
<!--                <a class="ui button">Log in</a>-->
<!--            </div>-->
<!--            <div class="item">-->
<!--                <a class="ui primary button">Sign Up</a>-->
<!--            </div>-->
<!--        </div>-->
        <div class="right menu">
            <div class="ui simple dropdown item">
                Nickname <i class="large user circle icon"></i>
                <div class="menu">
                    <img class="ui small circular image centered" src="/images/empty_avatar.png">
                    <br>
                    <a class="item">Мой портфель</a>
                    <a class="item">Админка</a>
                    <a class="item">Выход</a>
                </div>
            </div>
        </div>
    </div>
</div>

<style scoped>
    header {
        height: 52px;
        padding-bottom: 52px;
    }
    .logo {
        display: flex;
        align-items: center;
    }
    .logo img {
        width: 200px;
    }
    .dropdown .menu.visible {
        display: block;
    }
</style>