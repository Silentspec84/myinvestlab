<?php
namespace app\classes;

use app\classes\helpers\EnvironmentHelper;
use yii\base\ErrorException;
use yii\web\ErrorHandler as BaseErrorHandler;

/**
 * Переопределенный yii обработчик ошибок, чтобы можно было warning поставить на один уровень c fatal, например.
 *
 * Class ErrorHandler
 */
class ErrorHandler extends BaseErrorHandler
{
    /**
     * @var int задает какие типы ошибок будут приводить к ErrorException
     * и будут обрабатываться в методе @see handleError
     */
    public $types_to_exceptions = (E_ALL | E_STRICT);

    /**
     * @var int задает какие типы ошибок не прерывают работу приложения,
     * и будут записаны в self::$exceptions.
     * В режиме разработчика, будут отображаться внизу страницы c помощью @see ExceptionsWidget
     */
    public $types_error_continue_run_app = E_NOTICE;

    /** @var int задает какие типы ошибок отправлять в Sentry */
    public $types_error_to_write_to_sentry = (E_ALL & ~E_NOTICE | E_STRICT);

    /** @var array исключений, которые попадают под категорию $typesErrorContinueRunApp отображаются в режиме отладки */
    public static $exceptions_for_developer_mode = [];
    public static $ajax_exceptions_for_developer_mode = [];

    /**
     * Register this error handler
     */
    public function register()
    {
        error_reporting($this->types_to_exceptions);
        parent::register();
    }

    /**
     * Handles PHP execution errors such as warnings and notices.
     *
     * This method is used as a PHP error handler. It will simply raise an [[ErrorException]].
     *
     * @param int $code the level of the error raised.
     * @param string $message the error message.
     * @param string $file the filename that the error was raised in.
     * @param int $line the line number the error was raised at.
     * @return bool whether the normal error handler continues.
     *
     * @throws ErrorException
     */
    public function handleError($code, $message, $file, $line)
    {
        if (error_reporting() & $code) {
            // load ErrorException manually here because autoloading them will not work
            // when error occurs while autoloading a class
            if (!class_exists('yii\\base\\ErrorException', false)) {
                require_once  \Yii::getAlias('@app'). '/vendor/yiisoft/yii2/base/ErrorException.php';
            }
            $exception = new ErrorException($message, $code, $code, $file, $line);

            // in case error appeared in __toString method we can't throw any exception
            $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
            array_shift($trace);
            foreach ($trace as $frame) {
                if ($frame['function'] === '__toString') {
                    $this->handleException($exception);
                    if (defined('HHVM_VERSION')) {
                        flush();
                    }
                    exit(1);
                }
            }

            $exception_code = $exception->getCode();
            $exception_msg = $exception->getMessage();
            /** Отправка исключения в Sentry */
            if ($this->types_error_to_write_to_sentry & $exception_code) {
                $level_logger = $this->getLevelLoggerToExceptionCode($exception_code);
                \Yii::$level_logger($exception_msg, 'sentry');
            }
            /**
             * Выбрасываем исключение на ошибку, либо если ошибка некритичная, например E_NOTICE,
             * то записываем её для отображения в режиме разработчика
             */
            if ($this->types_error_continue_run_app & $exception_code) {
                self::$exceptions_for_developer_mode[$exception_msg] = $exception;
                if (\Yii::$app->request->isAjax) {
                    self::$ajax_exceptions_for_developer_mode[$exception_msg] = ['file' => $exception->getFile(), 'line' => $exception->getLine()];
                }
            } else {
                throw $exception;
            }
        }

        return false;
    }

    /**
     * Отправляет в sentry фаталы
     * @param \Exception $exception
     */
    public function handleException($exception)
    {
        if (\Yii::$app->params['environment']->isLocal()) {
            return parent::handleException($exception); // не шлем фаталы с локала в sentry
        }

        if (!in_array($exception->getCode() , [E_WARNING, E_NOTICE], true)) { // ворнинги и нотисы шлем $this->handleError, не будем дублировать их отправку
            $level_logger = $this->getLevelLoggerToExceptionCode($exception->getCode());
            \Yii::$level_logger($exception->getMessage(), 'sentry');
        }

        return parent::handleException($exception);
    }

    /**
     * Получить уровень логгера по коду исключения(ошибки)
     *
     * @param int $exception_code
     * @return string
     */
    private function getLevelLoggerToExceptionCode(int $exception_code): string
    {
        if ($exception_code === E_WARNING) {
            return 'warning';
        }
        if ($exception_code === E_NOTICE) {
            return 'info';
        }

        return 'error';
    }
}