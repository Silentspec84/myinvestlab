<?php

namespace app\classes\helpers;

/**
 * Класс SingletonTrait
 * Реализует паттерн синглтона
 *
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 * @package app\classes\helpers
 */
trait SingletonTrait {

    /** @var static */
    protected static $instance;

    /**
     * @param array $params
     * @return static
     */
    public static function i(...$params) {
        return static::$instance ?: static::$instance = new static(...$params);
    }

    /**
     * Конструктор
     * @param array $params
     */
    final private function __construct(...$params) {
        $this->init(...$params);
    }

    /** Инициализация
     * @param array $args
     */
    protected function init(...$args) {}

    final private function __clone() {}
}
