<?php

namespace app\classes\helpers;

use app\classes\strategies\BaseStrategy;
use app\models\Exchange;
use app\models\Instrument;
use app\models\Logs;
use app\models\TopCurrency;

class InstrumentParserHelper
{
    private $exchange;

    private $logger;

    private $instruments;

    private $base_currencies;

    public function __construct(Exchange $exchange, Logs $logger)
    {
        $this->exchange = $exchange;
        $this->logger = $logger;
        $this->instruments = $this->prepareInstruments();
        $this->base_currencies = ['RUB', 'USD', 'EUR', 'BTC', 'ETH', 'USDT']; //, 'LTC', 'EOS', 'BNB', 'BSV', 'XLM', 'XRP'
        $this->logger->saveMessage('Класс парсера цепочек успешно создан');
    }

    /**
     * Метод возвращает все возможные первые и последние пары для цепочки
     *
     * @return array
     */
    private function getFirstPairs()
    {
        $first_and_last_pairs = [];
        foreach ($this->instruments as $instrument_key => $instrument) {
            $currencies = explode('_', $instrument);
            if (!in_array($currencies[0], $this->base_currencies) && !in_array($currencies[1], $this->base_currencies)) {
                continue;
            }
            $first_and_last_pairs[$instrument_key] = $instrument;
        }
        return $first_and_last_pairs;
    }

    public function parseQuadripples(): array
    {
        $result = [];
        $count = 0;

        $first_pairs = $this->getFirstPairs();

        foreach ($first_pairs as $instrument => $instrument_key) {
            $first_pair = $this->getPairCurrencies($instrument);
            if (!in_array($first_pair[0], $this->base_currencies) && !in_array($first_pair[1], $this->base_currencies)) {
                continue;
            }
            foreach ($this->instruments as $instrument_next => $instrument_next_key) {
                $second_pair = $this->getPairCurrencies($instrument_next);
                if ($instrument === $instrument_next || (!in_array($first_pair[0], $second_pair, true) && !in_array($first_pair[1], $second_pair, true))) {
                    continue;
                }
                if (in_array($first_pair[0], $second_pair, true) && in_array($first_pair[1], $this->base_currencies)) {
                    if ($first_pair[0] === $second_pair[0]) {
                        foreach ($this->instruments as $instrument_last => $instrument_last_key) {
                            $third_pair = $this->getPairCurrencies($instrument_last);
                            if ($instrument_next === $instrument_last || !in_array($second_pair[1], $third_pair, true)) {
                                continue;
                            }
                            if ($second_pair[1] === $third_pair[0]) {
                                $last_pair = '';
                                if (array_key_exists($first_pair[1] . '_' . $third_pair[1], $this->instruments)) {
                                    $last_pair = $first_pair[1] . '_' . $third_pair[1];
                                }
                                if (array_key_exists($third_pair[1] . '_' . $first_pair[1], $this->instruments)) {
                                    $last_pair = $third_pair[1] . '_' . $first_pair[1];
                                }
                                if ($last_pair !== '') {
                                    $result[$count]['first_currency'] = $first_pair[1];
                                    $result[$count]['pattern'] = $first_pair[1] . '_' . $first_pair[0] . '-'
                                        . $second_pair[0] . '_' . $second_pair[1] . '-'
                                        . $third_pair[0] . '_' . $third_pair[1] . '-'
                                        . $third_pair[1] . '_' . $first_pair[1];
                                    $result[$count]['base'] = [$instrument, $instrument_next, $instrument_last, $last_pair];
                                    $result[$count]['dir'] = [
                                        $this->getDirection($first_pair[1], $instrument),
                                        $this->getDirection($second_pair[0], $instrument_next),
                                        $this->getDirection($third_pair[0], $instrument_last),
                                        $this->getDirection($third_pair[1], $last_pair),
                                    ];
                                    $count++;
                                    continue;
                                }
                                continue;
                            }
                            if ($second_pair[1] === $third_pair[1]) {
                                $last_pair = '';
                                if (array_key_exists($first_pair[1] . '_' . $third_pair[0], $this->instruments)) {
                                    $last_pair = $first_pair[1] . '_' . $third_pair[0];
                                }
                                if (array_key_exists($third_pair[0] . '_' . $first_pair[1], $this->instruments)) {
                                    $last_pair = $third_pair[0] . '_' . $first_pair[1];
                                }
                                if ($last_pair !== '') {
                                    $result[$count]['first_currency'] = $first_pair[1];
                                    $result[$count]['pattern'] = $first_pair[1] . '_' . $first_pair[0] . '-'
                                        . $second_pair[0] . '_' . $second_pair[1] . '-'
                                        . $third_pair[1] . '_' . $third_pair[0] . '-'
                                        . $third_pair[0] . '_' . $first_pair[1];
                                    $result[$count]['base'] = [$instrument, $instrument_next, $instrument_last, $last_pair];
                                    $result[$count]['dir'] = [
                                        $this->getDirection($first_pair[1], $instrument),
                                        $this->getDirection($second_pair[0], $instrument_next),
                                        $this->getDirection($third_pair[1], $instrument_last),
                                        $this->getDirection($third_pair[0], $last_pair),
                                    ];
                                    $count++;
                                    continue;
                                }
                                continue;
                            }
                        }
                    }
                    if ($first_pair[0] === $second_pair[1]) {
                        foreach ($this->instruments as $instrument_last => $instrument_last_key) {
                            $third_pair = $this->getPairCurrencies($instrument_last);
                            if ($instrument_next === $instrument_last || !in_array($second_pair[0], $third_pair, true)) {
                                continue;
                            }
                            if ($second_pair[0] === $third_pair[0]) {
                                $last_pair = '';
                                if (array_key_exists($first_pair[1] . '_' . $third_pair[1], $this->instruments)) {
                                    $last_pair = $first_pair[1] . '_' . $third_pair[1];
                                }
                                if (array_key_exists($third_pair[1] . '_' . $first_pair[1], $this->instruments)) {
                                    $last_pair = $third_pair[1] . '_' . $first_pair[1];
                                }
                                if ($last_pair !== '') {
                                    $result[$count]['first_currency'] = $first_pair[1];
                                    $result[$count]['pattern'] = $first_pair[1] . '_' . $first_pair[0] . '-'
                                        . $second_pair[1] . '_' . $second_pair[0] . '-'
                                        . $third_pair[0] . '_' . $third_pair[1] . '-'
                                        . $third_pair[1] . '_' . $first_pair[1];
                                    $result[$count]['base'] = [$instrument, $instrument_next, $instrument_last, $last_pair];
                                    $result[$count]['dir'] = [
                                        $this->getDirection($first_pair[1], $instrument),
                                        $this->getDirection($second_pair[1], $instrument_next),
                                        $this->getDirection($third_pair[0], $instrument_last),
                                        $this->getDirection($third_pair[1], $last_pair),
                                    ];
                                    $count++;
                                    continue;
                                }
                                continue;
                            }
                            if ($second_pair[0] === $third_pair[1]) {
                                $last_pair = '';
                                if (array_key_exists($first_pair[1] . '_' . $third_pair[0], $this->instruments)) {
                                    $last_pair = $first_pair[1] . '_' . $third_pair[0];
                                }
                                if (array_key_exists($third_pair[0] . '_' . $first_pair[1], $this->instruments)) {
                                    $last_pair = $third_pair[0] . '_' . $first_pair[1];
                                }
                                if ($last_pair !== '') {
                                    $result[$count]['first_currency'] = $first_pair[1];
                                    $result[$count]['pattern'] = $first_pair[1] . '_' . $first_pair[0] . '-'
                                        . $second_pair[1] . '_' . $second_pair[0] . '-'
                                        . $third_pair[1] . '_' . $third_pair[0] . '-'
                                        . $third_pair[0] . '_' . $first_pair[1];
                                    $result[$count]['base'] = [$instrument, $instrument_next, $instrument_last, $last_pair];
                                    $result[$count]['dir'] = [
                                        $this->getDirection($first_pair[1], $instrument),
                                        $this->getDirection($second_pair[1], $instrument_next),
                                        $this->getDirection($third_pair[1], $instrument_last),
                                        $this->getDirection($third_pair[0], $last_pair),
                                    ];
                                    $count++;
                                    continue;
                                }
                                continue;
                            }
                        }
                    }
                }
                if (in_array($first_pair[1], $second_pair, true) && in_array($first_pair[0], $this->base_currencies)) {
                    if ($first_pair[1] === $second_pair[0]) {
                        foreach ($this->instruments as $instrument_last => $instrument_last_key) {
                            $third_pair = $this->getPairCurrencies($instrument_last);
                            if ($instrument_next === $instrument_last || !in_array($second_pair[1], $third_pair, true)) {
                                continue;
                            }
                            if ($second_pair[1] === $third_pair[0]) {
                                $last_pair = '';
                                if (array_key_exists($first_pair[0] . '_' . $third_pair[1], $this->instruments)) {
                                    $last_pair = $first_pair[0] . '_' . $third_pair[1];
                                }
                                if (array_key_exists($third_pair[1] . '_' . $first_pair[0], $this->instruments)) {
                                    $last_pair =$third_pair[1] . '_' . $first_pair[0];
                                }
                                if ($last_pair !== '') {
                                    $result[$count]['first_currency'] = $first_pair[0];
                                    $result[$count]['pattern'] = $first_pair[0] . '_' . $first_pair[1] . '-'
                                        . $second_pair[0] . '_' . $second_pair[1] . '-'
                                        . $third_pair[0] . '_' . $third_pair[1] . '-'
                                        . $third_pair[1] . '_' . $first_pair[0];
                                    $result[$count]['base'] = [$instrument, $instrument_next, $instrument_last, $last_pair];
                                    $result[$count]['dir'] = [
                                        $this->getDirection($first_pair[0], $instrument),
                                        $this->getDirection($second_pair[0], $instrument_next),
                                        $this->getDirection($third_pair[0], $instrument_last),
                                        $this->getDirection($third_pair[1], $last_pair),
                                    ];
                                    $count++;
                                    continue;
                                }
                                continue;
                            }
                            if ($second_pair[1] === $third_pair[1]) {
                                $last_pair = '';
                                if (array_key_exists($first_pair[0] . '_' . $third_pair[0], $this->instruments)) {
                                    $last_pair = $first_pair[0] . '_' . $third_pair[0];
                                }
                                if (array_key_exists($third_pair[0] . '_' . $first_pair[0], $this->instruments)) {
                                    $last_pair = $third_pair[0] . '_' . $first_pair[0];
                                }
                                if ($last_pair !== '') {
                                    $result[$count]['first_currency'] = $first_pair[0];
                                    $result[$count]['pattern'] = $first_pair[0] . '_' . $first_pair[1] . '-'
                                        . $second_pair[0] . '_' . $second_pair[1] . '-'
                                        . $third_pair[1] . '_' . $third_pair[0] . '-'
                                        . $third_pair[0] . '_' . $first_pair[0];
                                    $result[$count]['base'] = [$instrument, $instrument_next, $instrument_last, $last_pair];
                                    $result[$count]['dir'] = [
                                        $this->getDirection($first_pair[0], $instrument),
                                        $this->getDirection($second_pair[0], $instrument_next),
                                        $this->getDirection($third_pair[1], $instrument_last),
                                        $this->getDirection($third_pair[0], $last_pair),
                                    ];
                                    $count++;
                                    continue;
                                }
                                continue;
                            }
                        }
                    }
                    if ($first_pair[1] === $second_pair[1]) {
                        foreach ($this->instruments as $instrument_last => $instrument_last_key) {
                            $third_pair = $this->getPairCurrencies($instrument_last);
                            if ($instrument_next === $instrument_last || !in_array($second_pair[1], $third_pair, true)) {
                                continue;
                            }
                            if ($second_pair[0] === $third_pair[0]) {
                                $last_pair = '';
                                if (array_key_exists($first_pair[0] . '_' . $third_pair[1], $this->instruments)) {
                                    $last_pair = $first_pair[0] . '_' . $third_pair[1];
                                }
                                if (array_key_exists($third_pair[1] . '_' . $first_pair[0], $this->instruments)) {
                                    $last_pair = $third_pair[1] . '_' . $first_pair[0];
                                }
                                if ($last_pair !== '') {
                                    $result[$count]['first_currency'] = $first_pair[0];
                                    $result[$count]['pattern'] = $first_pair[0] . '_' . $first_pair[1] . '-'
                                        . $second_pair[1] . '_' . $second_pair[0] . '-'
                                        . $third_pair[0] . '_' . $third_pair[1] . '-'
                                        . $third_pair[1] . '_' . $first_pair[0];
                                    $result[$count]['base'] = [$instrument, $instrument_next, $instrument_last, $last_pair];
                                    $result[$count]['dir'] = [
                                        $this->getDirection($first_pair[0], $instrument),
                                        $this->getDirection($second_pair[1], $instrument_next),
                                        $this->getDirection($third_pair[0], $instrument_last),
                                        $this->getDirection($third_pair[1], $last_pair),
                                    ];
                                    $count++;
                                    continue;
                                }
                                continue;
                            }
                            if ($second_pair[0] === $third_pair[1]) {
                                $last_pair = '';
                                if (array_key_exists($first_pair[0] . '_' . $third_pair[0], $this->instruments)) {
                                    $last_pair = $first_pair[0] . '_' . $third_pair[0];
                                }
                                if (array_key_exists($third_pair[0] . '_' . $first_pair[0], $this->instruments)) {
                                    $last_pair = $third_pair[0] . '_' . $first_pair[0];
                                }
                                if ($last_pair !== '') {
                                    $result[$count]['first_currency'] = $first_pair[0];
                                    $result[$count]['pattern'] = $first_pair[0] . '_' . $first_pair[1] . '-'
                                        . $second_pair[1] . '_' . $second_pair[0] . '-'
                                        . $third_pair[1] . '_' . $third_pair[0] . '-'
                                        . $third_pair[0] . '_' . $first_pair[0];
                                    $result[$count]['base'] = [$instrument, $instrument_next, $instrument_last, $last_pair];
                                    $result[$count]['dir'] = [
                                        $this->getDirection($first_pair[0], $instrument),
                                        $this->getDirection($second_pair[1], $instrument_next),
                                        $this->getDirection($third_pair[1], $instrument_last),
                                        $this->getDirection($third_pair[0], $last_pair),
                                    ];
                                    $count++;
                                    continue;
                                }
                                continue;
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }


    public function parseTripples(): array
    {
        $result = [];
        $count = 0;

        $first_pairs = $this->getFirstPairs();

        foreach ($first_pairs as $instrument => $instrument_key) {
            $first_pair = $this->getPairCurrencies($instrument);
            if (!in_array($first_pair[0], $this->base_currencies) && !in_array($first_pair[1], $this->base_currencies)) {
                continue;
            }
            foreach ($this->instruments as $instrument_next => $instrument_next_key) {
                $second_pair = $this->getPairCurrencies($instrument_next);
                if ($instrument === $instrument_next || (!in_array($first_pair[0], $second_pair, true) && !in_array($first_pair[1], $second_pair, true))) {
                    continue;
                }
                if (in_array($first_pair[0], $second_pair, true) && in_array($first_pair[1], $this->base_currencies)) {
                    $result[$count]['first_currency'] = $first_pair[1];
                    if ($first_pair[0] === $second_pair[0]) {
                        $last_pair = '';
                        if (array_key_exists($second_pair[1] . '_' . $first_pair[1], $this->instruments)) {
                            $last_pair = $second_pair[1] . '_' . $first_pair[1];
                        }
                        if (array_key_exists($first_pair[1] . '_' . $second_pair[1], $this->instruments)) {
                            $last_pair = $first_pair[1] . '_' . $second_pair[1];
                        }
                        if ($last_pair !== '') {
                            $result[$count]['pattern'] = $first_pair[1] . '_' . $first_pair[0] . '-'
                                . $second_pair[0] . '_' . $second_pair[1] . '-'
                                . $second_pair[1] . '_' . $first_pair[1];
                            $result[$count]['base'] = [$instrument, $instrument_next, $last_pair];
                            $result[$count]['dir'] = [
                                $this->getDirection($first_pair[1], $instrument),
                                $this->getDirection($second_pair[0], $instrument_next),
                                $this->getDirection($second_pair[1], $last_pair),
                            ];
                            $count++;
                            continue;
                        }
                        continue;
                    }
                    if ($first_pair[0] === $second_pair[1]) {
                        $last_pair = '';
                        if (array_key_exists($second_pair[0] . '_' . $first_pair[1], $this->instruments)) {
                            $last_pair = $second_pair[0] . '_' . $first_pair[1];
                        }
                        if (array_key_exists($first_pair[1] . '_' . $second_pair[0], $this->instruments)) {
                            $last_pair = $first_pair[1] . '_' . $second_pair[0];
                        }
                        if ($last_pair !== '') {
                            $result[$count]['pattern'] = $first_pair[1] . '_' . $first_pair[0] . '-'
                                . $second_pair[1] . '_' . $second_pair[0] . '-'
                                . $second_pair[0] . '_' . $first_pair[1];
                            $result[$count]['base'] = [$instrument, $instrument_next, $last_pair];
                            $result[$count]['dir'] = [
                                $this->getDirection($first_pair[1], $instrument),
                                $this->getDirection($second_pair[1], $instrument_next),
                                $this->getDirection($second_pair[0], $last_pair),
                            ];
                            $count++;
                            continue;
                        }
                        continue;
                    }
                }
                if (in_array($first_pair[1], $second_pair, true) && in_array($first_pair[0], $this->base_currencies)) {
                    $result[$count]['first_currency'] = $first_pair[0];
                    if ($first_pair[1] === $second_pair[0]) {
                        $last_pair = '';
                        if (array_key_exists($second_pair[1] . '_' . $first_pair[0], $this->instruments)) {
                            $last_pair =$second_pair[1] . '_' . $first_pair[0];
                        }
                        if (array_key_exists($first_pair[0] . '_' . $second_pair[1], $this->instruments)) {
                            $last_pair = $first_pair[0] . '_' . $second_pair[1];
                        }
                        if ($last_pair !== '') {
                            $result[$count]['pattern'] = $first_pair[0] . '_' . $first_pair[1] . '-'
                                . $second_pair[0] . '_' . $second_pair[1] . '-'
                                . $second_pair[1] . '_' . $first_pair[0];
                            $result[$count]['base'] = [$instrument, $instrument_next, $last_pair];
                            $result[$count]['dir'] = [
                                $this->getDirection($first_pair[0], $instrument),
                                $this->getDirection($second_pair[0], $instrument_next),
                                $this->getDirection($second_pair[1], $last_pair),
                            ];
                            $count++;
                            continue;
                        }
                        continue;
                    }
                    if ($first_pair[1] === $second_pair[1]) {
                        $last_pair = '';
                        if (array_key_exists($second_pair[1] . '_' . $first_pair[1], $this->instruments)) {
                            $last_pair = $second_pair[1] . '_' . $first_pair[1];
                        }
                        if (array_key_exists($first_pair[1] . '_' . $second_pair[1], $this->instruments)) {
                            $last_pair = $first_pair[1] . '_' . $second_pair[1];
                        }
                        if ($last_pair !== '') {
                            $result[$count]['pattern'] = $first_pair[0] . '_' . $first_pair[1] . '-'
                                . $second_pair[1] . '_' . $second_pair[0] . '-'
                                . $second_pair[0] . '_' . $first_pair[0];
                            $result[$count]['base'] = [$instrument, $instrument_next, $last_pair];
                            $result[$count]['dir'] = [
                                $this->getDirection($first_pair[0], $instrument),
                                $this->getDirection($second_pair[1], $instrument_next),
                                $this->getDirection($second_pair[0], $last_pair),
                            ];
                            $count++;
                            continue;
                        }
                        continue;
                    }
                }
                continue;
            }
        }
        return $result;
    }

    private function getDirection($first_currency, $pair)
    {
        $currencies = explode('_', $pair);
        if ($first_currency !== $currencies[0]) {
            return in_array($this->exchange->name, ['exmo', 'Binance', 'HitBTC']) ? BaseStrategy::BUY : BaseStrategy::SELL;
        }
        return in_array($this->exchange->name, ['exmo', 'Binance', 'HitBTC']) ? BaseStrategy::SELL : BaseStrategy::BUY;
    }

    private function prepareInstruments(): array
    {
        $result = [];
        $this->logger->saveMessage('Получаем инструменты из бд');
        $instruments = Instrument::find()->where(['exchange_id' => $this->exchange->id, 'status' => Instrument::STATUS_ON])->all();
        $this->logger->saveMessage('Получено ' . count($instruments) . ' инструментов из таблицы instruments');
        /** @var Instrument $instrument */
        foreach ($instruments as $instrument) {
            $currencies = explode('_', $instrument->name);
            $result[$instrument->name] = $instrument->name;
        }
        return $result;
    }

    private function getPairCurrencies(string $pair): array
    {
        return explode('_', $pair);
    }
}