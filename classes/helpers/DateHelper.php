<?php

namespace app\classes\helpers;

/** Хелпер по ра работе с датой и форматами дат */
class DateHelper
{
    /**
     * Проверка что date_version это timestamp в секундах(10значный int)
     * @param $date
     * @return bool
     */
    public static function isDateInTimestamp($date): bool
    {
        $date = (int)$date;

        return strlen((string)$date) === 10;
    }

    /**
     * Проверка что date это timestamp в микросекундах(13значный int)
     * @param $date
     * @return bool
     */
    public static function isDateInTimestampMS($date): bool
    {
        $date = (int)$date;

        return strlen((string)$date) === 13;
    }
}