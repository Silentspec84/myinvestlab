<?php

namespace app\controllers;

use app\classes\connectors\ApiConnectorInterface;
use app\components\AccessRule;
use app\models\Exchange;
use app\models\forms\RegisterForm;
use app\models\Instrument;
use app\models\User;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\Url;


/**
 * Class SiteApiController
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */

class SiteApiController extends BaseApiController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['get-menu-data', 'register', 'login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['get-menu-data', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    $this->redirect('/');
                }
            ],
        ];
    }

    public function actionGetMenuData()
    {
        if (!$this->validateRequestWithOnlyCsrf()) {
            return $this->response;
        }

        if (Yii::$app->user->isGuest) {
            return $this->response->setContent([
                'user' => ['id' => 0],
            ]);
        } else {
            $user = User::findOne(Yii::$app->user->id);
        }

        return $this->response->setContent([
            'user' => $user,
        ]);
    }

    public function actionLogin()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'login_form' => ['type' => 'array', 'required' => true,
                'config' => [
                    'email' => ['type' => 'string', 'required' => false, 'rules' => ['is_correct_email' => []]],
                    'username' => ['type' => 'string', 'required' => false],
                    'login' => ['type' => 'string', 'required' => true],
                    'password' => ['type' => 'string', 'required' => true],
                    'remember' => ['type' => 'boolean', 'required' => true],
                ]
            ],
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $user = User::find()
            ->where(
                [
                    'or',
                    ['login_name' => $this->request_post['login_form']['login']],
                    ['email' => $this->request_post['login_form']['login']]
                ]
            )->one();

        if (!$user) {
            if ($this->request_post['login_form']['email'] !== "") {
                return $this->response->addError('Неверный email, попробуйте снова', 'email');
            }
            return $this->response->addError('Неверный логин, попробуйте снова', 'username');
        }

        if (!$user->validatePassword($this->request_post['login_form']['password'])) {
            return $this->response->addError('Неверный пароль, попробуйте снова', 'password');
        }

        $duration = $this->request_post['login_form']['remember'] ? 3600 * 30 * 24 : 0;

        Yii::$app->user->login($user, $duration);

        return $this->response->setContent([
            'user' => $user,
        ]);
    }

    /*
     * Сама регистрация
     */
    public function actionRegister()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'register_form' => ['type' => 'array', 'required' => true,
                'config' => [
                    'email' => ['type' => 'string', 'required' => true, 'rules' => ['is_correct_email' => []]],
                    'login_name' => ['type' => 'string', 'required' => true],
                    'repeat_password' => ['type' => 'string', 'required' => true],
                    'password' => ['type' => 'string', 'required' => true],
                ]
            ],
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $transaction = Yii::$app->db->beginTransaction();
            $user = new User();
            $user->login_name = $this->request_post['register_form']['login_name'];
            $user->email = $this->request_post['register_form']['email'];
            $user->unsafe_password = $this->request_post['register_form']['password'];
            $user->getHashForUser();
            $user->role = User::ROLE_USER;
            $user->status = User::STATUS_UNCONFIRMED;

            if (!$user->save()) {
                $transaction->rollBack();
                return $this->response->addError($user->errors);
            }

            $content['link'] = Url::to(['site/confirm-email', 'hash' => $user->hash], true);
            $content['login_name'] = $user->login_name;

            if (!Yii::$app->mailer->compose('confirmation', ['content' => $content])
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setTo($user->email)
                ->setSubject('Регистрация на сайте InvestLab.ru')
                ->send()) {
                $transaction->rollBack();
            }
        try {
            $transaction->commit();
        } catch (Exception $e) {
            return $this->response->addError("Не удалось произвести сохранение!");
        }

        return $this->response;
    }

}
