<?php

namespace app\controllers;

use app\components\Logger;
use app\classes\helpers\SlackHelper;
use app\components\rabbitmq\RabbitSenderBase;
use app\components\rabbitmq\RabbitSenderMail;
use Yii;
use app\models\User;
use yii\base\Exception;
use yii\filters\AccessControl;
use app\components\AccessRule;
use app\models\Company;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

/**
 * Class BaseController
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 *
 */
class BaseController extends Controller
{
    /** @var  User */
    protected $user;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN, User::ROLE_USER],
                    ]
                ],
                'denyCallback' => function() {
                    if ($this->user->status === User::STATUS_INACTIVE) {
                        Yii::$app->user->logout();
                        $this->redirect('/login');
                    }
                    if ($this->user->role === User::STATUS_UNCONFIRMED) {
                        $this->redirect('/login');
                    }
                    Yii::$app->getSession()->setFlash('error', 'Недостаточно прав для просмотра раздела');
                    $this->redirect('/');
                }
            ],
        ];
    }

    /**
     * @param $action
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->identity) {
            $this->user = Yii::$app->user->identity;
        }

        return parent::beforeAction($action);
    }

    /**
     * Передаёт переменные в шаблон для использования в JS (будут доступны через this.$config)
     * переопределяется в каждом контроллере, где это необходимо
     */
    public static function getJsConfig()
    {
        return [
            'ROLE_ADMIN'  => User::ROLE_ADMIN,
            'ROLE_MODERATOR'  => User::ROLE_MODERATOR,
            'ROLE_USER'  => User::ROLE_USER
        ];
    }
}
